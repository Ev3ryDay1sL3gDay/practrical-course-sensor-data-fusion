
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from nuscenes.nuscenes import NuScenes
from nuscenes.utils.data_classes import LidarPointCloud, Box
from nuscenes.utils.geometry_utils import view_points, box_in_image, BoxVisibility, transform_matrix
from nuscenes.nuscenes import NuScenesExplorer
import time
import multiprocessing
from pyquaternion import Quaternion
import subprocess
# import os
# print(os.getcwd())
nusc = NuScenes(version='v1.0-mini', dataroot='../../../Dataset/nuscenes')

scene_index = int(input("Enter Scene index (0..9) : "))
my_scene = nusc.scene[scene_index]

sample_token = my_scene['first_sample_token']
sample_tokens = []
sample_records = []
for i in range(my_scene['nbr_samples']):
    sample_tokens.append(sample_token)
    sample_records.append(nusc.get('sample', sample_token))
    sample_token = sample_records[-1]['next']

# print(sample_tokens)
# print(my_scene)
# sd_record = nusc.get('sample_data', my_scene)
# nusc.render_sample_data(my_scene['first_sample_token']['data']['LIDAR_TOP'])

IMG_DIR = "pics/"
VIDEO_DIR = "videos/"
SCENE_INDEX_PREFIX="_scene_"
VIDEO_FILETYPE_EXTENSION=".mp4"
SAMPLE_TOKEN_PREFIX="_sample_token_"
IMAGE_FILETYPE_EXTENSION= ".png"
PLT_WIDTH=100
PLT_HEIGHT=100
def get_filename(scene_index,sample_token,index):
    return IMG_DIR+SCENE_INDEX_PREFIX+str(scene_index)+SAMPLE_TOKEN_PREFIX+str(index).zfill(4)+IMAGE_FILETYPE_EXTENSION

ax = plt.gca()
def render_sample(sample_token_index, show=True):
    
    plt.xlim([-70,70])
    plt.ylim([-70,70])
    plt.axis('off')
    sample_token = sample_tokens[sample_token_index]
    sample_record = sample_records[sample_token_index]
    sd_record = nusc.get('sample_data', sample_record['data']['LIDAR_TOP'])
    # nusc.render_sample_data(my_sample_record['data']['LIDAR_TOP'])
    sample_rec = nusc.get('sample', sd_record['sample_token'])
    chan = sd_record['channel']
    ref_chan = 'LIDAR_TOP'
    ref_sd_token = sample_rec['data'][ref_chan]
    ref_sd_record = nusc.get('sample_data', ref_sd_token)
    pc, times = LidarPointCloud.from_file_multisweep(nusc=nusc,sample_rec=sample_rec, chan=chan, ref_chan=ref_chan, nsweeps=1)
    velocities=None
    cs_record = nusc.get('calibrated_sensor', ref_sd_record['calibrated_sensor_token'])
    pose_record = nusc.get('ego_pose', ref_sd_record['ego_pose_token'])
    ref_to_ego = transform_matrix(translation=cs_record['translation'],
                                  rotation=Quaternion(cs_record["rotation"]))
    ego_yaw = Quaternion(pose_record['rotation']).yaw_pitch_roll[0]
    rotation_vehicle_flat_from_vehicle = np.dot(
        Quaternion(scalar=np.cos(ego_yaw / 2), vector=[0, 0, np.sin(ego_yaw / 2)]).rotation_matrix,
        Quaternion(pose_record['rotation']).inverse.rotation_matrix)
    vehicle_flat_from_vehicle = np.eye(4)
    vehicle_flat_from_vehicle[:3, :3] = rotation_vehicle_flat_from_vehicle
    viewpoint = np.dot(vehicle_flat_from_vehicle, ref_to_ego)
    
    points = view_points(pc.points[:3, :], viewpoint, normalize=False)
    dists = np.sqrt(np.sum(pc.points[:2, :] ** 2, axis=0))
    colors = np.minimum(1, dists / 40 / np.sqrt(2))
    point_scale = 0.2
    scatter = ax.scatter(points[0, :], points[1, :], c=colors, s=point_scale)
    ax.plot(0, 0, 'x', color='red')
    _, boxes, _ = nusc.get_sample_data(ref_sd_token, box_vis_level=BoxVisibility.ANY,
                                       use_flat_vehicle_coordinates=True)
    for box in boxes:
        c = np.array(NuScenesExplorer.get_color(box.name)) / 255.0
        box.render(ax, view=np.eye(4), colors=(c, c, c))
    plt.figure(1).text(.5,.05, "sample_index:"+str(sample_token_index))
    plt.savefig(get_filename(scene_index, sample_token, sample_token_index))
    if show:
        plt.show()
    plt.cla()
    print("rendered sample_token_index "+str(sample_token_index))
    return

def render_scene():
    plt.figure(1).suptitle(my_scene['description'])
    for i in range(my_scene['nbr_samples']):
        render_sample(i, show=False)

def render_scene_parallel():
    jpbs = []
    for i in range(my_scene['nbr_samples']):
        p = multiprocessing.Process(target=render_sample, args=(i,))
        p.start()


def png2mp4(scene_index):
    image_command = IMG_DIR+SCENE_INDEX_PREFIX+str(scene_index)+SAMPLE_TOKEN_PREFIX+"%4d"+IMAGE_FILETYPE_EXTENSION
    video_command = VIDEO_DIR+SCENE_INDEX_PREFIX+str(scene_index)+VIDEO_FILETYPE_EXTENSION
    subprocess.run(["ffmpeg",
                    "-r:v",
                    "10",
                    "-i",
                    image_command,
                    "-codec:v",
                    "libx264",
                    "-preset",
                    "veryslow",
                    "-pix_fmt",
                    "yuv420p",
                    "-crf",
                    "28",
                    video_command])

if __name__ == "__main__":
    if input("Render Scene? (y/n)") == "y":
        if input("In parallel? (y/n)") == "y":
            render_scene_parallel()
        else:
            render_scene()
    else:
        print("Exiting, nothing to do")
